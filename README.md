<h1 align="center">SavvyTechTask</h1>

## requirement
* php >= 8 (8.1 preferred)
* composer needed [see here](https://getcomposer.org/download/)

## installation
* part1 : clone and install project: 
  * run `composer install` command 
  * copy environment by `cp .env.example .env`


* part2 : make database and user
    * in <b>MySQL (MariaDB)</b>:
      * `CREATE DATABASE '[DB_NAME]';`
      * `CREATE USER '[USERNAME]'@localhost IDENTIFIED BY '[PASSWORD]';`
      * `GRANT ALL PRIVILEGES ON '[DB_NAME]'.* TO '[USERNAME]'@localhost;`
    * in <b>Postgresql</b>:
      * `CREATE DATABASE [DB_NAME];`
      * `CREATE USER [USERNAME] WITH PASSWORD '[PASSWORD]';`
      * `GRANT ALL PRIVILEGES ON DATABASE [DB_NAME] TO [USERNAME];`


* part3 : setup project
  * run `php artisan key:generate` command
  * edit <b>.env</b> file:
    * DB_DATABASE=`[DB_NAME]`
    * DB_USERNAME=`[USERNAME]`
    * DB_PASSWORD=`[PASSWORD]`
  * if you use <b>Postgresql</b>:
    * DB_CONNECTION = `pgsql`
    * DB_PORT=`5432`
  * run `php artisan migrate` command
    * for using seeder : run `php artisan migrate --seed` command

    

## postman collection link:
 * [here](https://www.getpostman.com/collections/37a4a72e266e3089a3cb)
