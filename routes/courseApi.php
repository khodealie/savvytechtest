<?php

use App\Http\Controllers\CourseController;
use Illuminate\Support\Facades\Route;

// used for the list of routes related to courses

Route::get('', [CourseController::class, 'getAllCourses']);
Route::get('sales-list', [CourseController::class, 'getSaleListOfCourses']);
