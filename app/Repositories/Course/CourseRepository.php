<?php

namespace App\Repositories\Course;

use App\Models\Course;
use App\Repositories\Interfaces\CourseRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class CourseRepository implements CourseRepositoryInterface
{

    //to change sortBase name to column name of table (date => updated_at)
    private function convertSortBaseToColumnNameOfTable($sortBase): string
    {
        return match ($sortBase) {
            'date' => 'updated_at',
            default => $sortBase
        };
    }

    //get all courses via custom sort
    public function allCourses($sortBase = 'rate', $inOrder = 'desc'): Collection
    {
        return Course::orderBy($this->convertSortBaseToColumnNameOfTable($sortBase), $inOrder)->get();
    }
}
