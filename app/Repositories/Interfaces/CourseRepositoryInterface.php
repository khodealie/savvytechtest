<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Collection;

interface CourseRepositoryInterface
{
    public function allCourses($sortBase = 'rate', $inOrder = 'desc'): Collection;
}
