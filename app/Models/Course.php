<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Course extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'price', 'rate', 'updated_at'];

    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(Order::class, 'order_items')->withPivot('price')->withTimestamps();
    }

    //this accessor used for get sales list of each course

    public function saleListPerMonth(): Attribute
    {
        return new Attribute(function () {
            $result = [];
            $orders = $this->orders()->get();
            foreach ($orders as $order) {
                $date = new Carbon($order->created_at);
                $result['y' . $date->year][$date->englishMonth]['salesCount'] =
                    ($result['y' . $date->year][$date->englishMonth]['salesCount'] ?? 0) + 1;
                $result['y' . $date->year][$date->englishMonth]['salesAmount'] =
                    ($result['y' . $date->year][$date->englishMonth]['salesAmount'] ?? 0) + $order->pivot->price;
            }
            return $result;
        });
    }
}
