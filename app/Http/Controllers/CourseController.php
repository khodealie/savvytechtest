<?php

namespace App\Http\Controllers;

use App\Http\Resources\GeneralCourseCollection;
use App\Http\Resources\SaleCourseCollection;
use App\Repositories\Interfaces\CourseRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CourseController extends Controller
{
    //make courseRepository
    public function __construct(private CourseRepositoryInterface $courseRepository)
    {
    }

    //to get all courses and sort by price, rate and date
    public function getAllCourses(Request $request): GeneralCourseCollection
    {
        $request->validate([
            'base' => [Rule::in(['price', 'rate', 'date'])],
            'sort' => [Rule::in('asc', 'desc')]
        ]);
        return new GeneralCourseCollection($this->courseRepository->allCourses($request->base ?? 'rate', $request->sort ?? 'desc'));
    }

    //to get all courses via sales list of them
    public function getSaleListOfCourses(): SaleCourseCollection
    {
        return new SaleCourseCollection($this->courseRepository->allCourses());
    }
}
