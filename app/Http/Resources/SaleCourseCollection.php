<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SaleCourseCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */

    // for use custom wrap in collection mode

    public static $wrap = 'courses';

    public function toArray($request)
    {
        return SaleCourseRes::collection($this->collection);
    }
}
