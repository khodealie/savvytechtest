<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SaleCourseRes extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */

    public static $wrap = 'course';

    public function toArray($request)
    {
        return [
            'id' => $this['id'],
            'saleListPerMonth' => $this['saleListPerMonth'],
        ];
    }
}
