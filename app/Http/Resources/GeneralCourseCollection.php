<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class GeneralCourseCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    // for use custom wrap in collection mode

    public static $wrap = 'courses';

    public function toArray($request)
    {
        return GeneralCourseRes::collection($this->collection);
    }
}
