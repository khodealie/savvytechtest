<?php

namespace Tests\Feature;

use Tests\TestCase;

class CoursesTest extends TestCase
{
    private function makeHeadersForApi(): array
    {
        return $this->transformHeadersToServerVars(['Accept' => 'application/json', 'Content-Type' => 'application/json']);
    }

    /** @test */
    public function getAllCoursesWithSalesList()
    {
        $response = $this->call('GET', '/api/courses/sales-list', server: $this->makeHeadersForApi());
        $response->assertStatus(200);
    }

    /** @test */
    public function getAllCoursesDefault()
    {
        $response = $this->call('GET', '/api/courses/', server: $this->makeHeadersForApi());
        $response->assertStatus(200);
    }

    /** @test */
    public function getAllCoursesRateAsc()
    {
        $response = $this->call('GET', '/api/courses/', ['base' => 'rate', 'sort' => 'asc'], server: $this->makeHeadersForApi());
        $response->assertStatus(200);
    }

    /** @test */
    public function getAllCoursesRateDesc()
    {
        $response = $this->call('GET', '/api/courses/', ['base' => 'rate', 'sort' => 'desc'], server: $this->makeHeadersForApi());
        $response->assertStatus(200);
    }

    /** @test */
    public function getAllCoursesPriceAsc()
    {
        $response = $this->call('GET', '/api/courses/', ['base' => 'price', 'sort' => 'asc'], server: $this->makeHeadersForApi());
        $response->assertStatus(200);
    }

    /** @test */
    public function getAllCoursesPriceDesc()
    {
        $response = $this->call('GET', '/api/courses/', ['base' => 'price', 'sort' => 'desc'], server: $this->makeHeadersForApi());
        $response->assertStatus(200);
    }

    /** @test */
    public function getAllCoursesDateAsc()
    {
        $response = $this->call('GET', '/api/courses/', ['base' => 'date', 'sort' => 'asc'], server: $this->makeHeadersForApi());
        $response->assertStatus(200);
    }

    /** @test */
    public function getAllCoursesDateDesc()
    {
        $response = $this->call('GET', '/api/courses/', ['base' => 'date', 'sort' => 'desc'], server: $this->makeHeadersForApi());
        $response->assertStatus(200);
    }

    /** @test */
    public function getAllCoursesRateViaFakeSort()
    {
        $response = $this->call('GET', '/api/courses/', ['base' => 'date', 'sort' => 'acs'], server: $this->makeHeadersForApi());
        $response->assertStatus(422);
    }

    /** @test */
    public function getAllCoursesFakeBaseDesc()
    {
        $response = $this->call('GET', '/api/courses/', ['base' => 'updated_at', 'sort' => 'desc'], server: $this->makeHeadersForApi());
        $response->assertStatus(422);
    }
}
