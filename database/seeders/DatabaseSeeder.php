<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $courses = Course::factory(10)->create();
        for ($count = 0; $count < 50; $count++) {
            $randomCourses = $courses->random(fake()->numberBetween(1, 4));
            $order = Order::create([]);
            $totalPrice = 0;
            foreach ($randomCourses as $randomCourse) {
                $newPrice = round(fake()->numberBetween($randomCourse->price * 0.8, $randomCourse->price), -3);
                $order->courses()->attach($randomCourse, ['price' => $newPrice]);
                $totalPrice += $newPrice;
            }
            $order->total_price = $totalPrice;
            $originalTotalPrice = $randomCourses->sum('price');
            $order->discount = $originalTotalPrice - $totalPrice;
            $order->created_at = Carbon::now()->subWeeks($count);
            $order->save();
        }
    }
}
