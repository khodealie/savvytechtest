<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Course>
 */
class CourseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => fake()->words(fake()->numberBetween(2, 5), true),
            'price' => (fake()->numberBetween(10, 1000) * 1000),
            'rate' => fake()->randomFloat(1, 1, 5),
            'updated_at' => Carbon::parse('last Friday')->subWeeks(fake()->numberBetween(1, 10))
        ];
    }
}
